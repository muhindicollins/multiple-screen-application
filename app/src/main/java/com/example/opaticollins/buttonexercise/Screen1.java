package com.example.opaticollins.buttonexercise;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;

/**
 * Created by opaticollins on 4/28/2017.
 */
public class Screen1 extends AppCompatActivity {

    protected  void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.main);

        Button btn3=(Button)findViewById(R.id.main);

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Screen1.this,MainActivity.class));
            }
        });

    }
}
