package com.example.opaticollins.buttonexercise;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by opaticollins on 4/28/2017.
 */
public class Screen2 extends AppCompatActivity {
    protected  void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.sub);

        Button btn3=(Button)findViewById(R.id.sub);

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Screen2.this,MainActivity.class));
            }
        });

    }
}
